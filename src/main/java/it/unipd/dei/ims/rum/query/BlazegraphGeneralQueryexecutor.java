package it.unipd.dei.ims.rum.query;

import java.util.Properties;

import org.openrdf.model.Model;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.model.impl.StatementImpl;
import org.openrdf.model.impl.TreeModel;
import org.openrdf.query.BindingSet;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.query.TupleQuery;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.sail.SailException;

import com.bigdata.journal.Options;
import com.bigdata.rdf.sail.BigdataSail;
import com.bigdata.rdf.sail.BigdataSailRepository;

import it.unipd.dei.ims.rum.utilities.BlazegraphUsefulMethods;

/** Class to execute one RDF query
 * */
public class BlazegraphGeneralQueryexecutor {

	public static void main(String[] args) throws RepositoryException, SailException {

		final Properties props = new Properties();
		props.put(Options.BUFFER_MODE, "DiskRW"); // persistent file system located journal
		// journal file location
		String databasePath = "/Users/dennisdosso/Documents/RDF_DATASETS/LinkedMDB/RDF_database/dataset.jnl";
		String outputPath = "/Users/dennisdosso/Documents/RDF_DATASETS/LinkedMDB/raw_data/test.ttl";
		
		props.put(Options.FILE, databasePath); 

		final BigdataSail sail = new BigdataSail(props); // instantiate a sail
		final Repository repo = new BigdataSailRepository(sail); // create a Sesame repository

		repo.initialize();

		try {
			// open repository connection
			RepositoryConnection cxn;

			// open connection
			if (repo instanceof BigdataSailRepository) {
				cxn = ((BigdataSailRepository) repo).getReadOnlyConnection();
			} else {
				cxn = repo.getConnection();
			}

			// evaluate sparql query
			try {

				//sparql query to submit
				String sparqlQuery = "select ?s ?p ?o where {?s ?p ?o} limit 100 offset 10";
				
				sparqlQuery = "select ?s  where {?s <http://data.linkedmdb.org/resource/movie/director_name> \"Wes Anderson\"} limit 100";
				sparqlQuery = "select ?s  where {?s <http://data.linkedmdb.org/resource/movie/actor_name> \"Owen Wilson\"} limit 100";
				final TupleQuery tq = cxn.prepareTupleQuery(QueryLanguage.SPARQL, sparqlQuery);
				final TupleQuery tupleQuery = cxn
						.prepareTupleQuery(QueryLanguage.SPARQL,
								//								"select ?s ?p ?o where { ?s ?p ?o . } LIMIT 400 OFFSET 1000000");//to list all the triples
								//								"select ?s ?p ?o where { <http://rdf.disgenet.org/v5.0.0/void/HPO> ?p ?o . }");
								//								"SELECT (count(*) AS ?count) { ?s ?p ?o .}");//to have the count of all the triples
								//								"SELECT DISTINCT (?p AS ?DistinctEdges)  { ?s ?p ?o } LIMIT 200");//to have the list of all distinct labels
								//								"SELECT p? (count(distinct ?p) as ?count) {?s ?p ?o}");
								//								"select ?s ?p ?o where { ?s ?p <IMDB:/Miss_Jerry> . } LIMIT 50");//query about Audrey Hepburn in IMDB 
								//								"select ?s ?p ?o where { <IMDB:/nm0000030> ?p ?o FILTER (str(?o) = \"IMDB:/Audrey Hepburn\") . } LIMIT 50");//query about Audrey Hepburn in IMDB 
								"select ?p ?o where { <https://www.imdb.com/ordering/tt0017048/1> ?p ?o . } LIMIT 100");
				//								"select ?s ?p where { ?s ?p <https://www.imdb.com/title/tt0024150> . } LIMIT 100");

				TupleQueryResult res = tq.evaluate();
				
				Model subgraph = new TreeModel ();
				
				int counter = 0;
				try {
					while (res.hasNext()) {
						BindingSet bindingSet = res.next();

						//						Value subject = bindingSet.getValue("s");
//						Value predicate = bindingSet.getValue("p");
//						Value object = bindingSet.getValue("o");
//						
//						Statement stat = new StatementImpl((URI) subject, (URI) predicate, object);
//						subgraph.add(stat);
						
						counter++;
						System.out.println(counter + ": " + bindingSet);
					}
					System.out.println("triples: " + counter);
					
//					BlazegraphUsefulMethods.printTheDamnGraph(subgraph, outputPath);
				} finally {
					res.close();
				}

			} catch (MalformedQueryException e) {
				e.printStackTrace();
			} catch (QueryEvaluationException e) {
				e.printStackTrace();
			} finally {
				// close the repository connection
				cxn.close();
			}

		} finally {
			repo.shutDown();
		}
	}
}
