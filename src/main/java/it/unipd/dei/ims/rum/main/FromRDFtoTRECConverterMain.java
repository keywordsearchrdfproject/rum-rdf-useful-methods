package it.unipd.dei.ims.rum.main;

import java.io.IOException;
import java.util.Map;

import it.unipd.dei.ims.rum.convertion.FromRDFtoTRECConverter;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;

public class FromRDFtoTRECConverterMain {

	public static void main(String[] args) throws IOException {

		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/trec.properties"); 

		String outputDirectory = map.get("trec.output.directory");
		String jdbcConnection = map.get("jdbc.connecting.string");;
		String jdbcDriver = map.get("jdbc.driver");

		FromRDFtoTRECConverter converter = new FromRDFtoTRECConverter(outputDirectory, jdbcConnection, jdbcDriver);

		converter.translateGraphsToTRECDocuments();
		System.out.print("done");
	}

}
