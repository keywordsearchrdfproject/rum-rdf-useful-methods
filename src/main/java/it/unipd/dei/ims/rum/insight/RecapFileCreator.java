package it.unipd.dei.ims.rum.insight;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;
import it.unipd.dei.ims.rum.utilities.UsefulConstants;

/** This class reads the information contained in 
 * different directories in pool files in 
 * order to write a csv file with the recap
 * of the performances of our algorithms.
 * <p>*/
public class RecapFileCreator {

	/** Path of the queries of the pipeline
	 * */
	private String mainQueryDirectory;

	/** Set to true if we are working with TSA, which is 
	 * a method that combines with other ranking methods.
	 * */
	private boolean isTSA;

	/** The threshold for the signal to noise ration
	 * that has been used*/
	private double lambda;

	/** Path of the file where to write the information*/
	private String outputFile;

	private String pipeline;

	/** Map with the properties*/
	private Map<String, String> map;

	public RecapFileCreator() {
		try {
			this.map = PropertiesUsefulMethods.
					getSinglePropertyFileMap("properties/RecapFileCreator.properties");

			isTSA = Boolean.parseBoolean(map.get("is.TSA"));
			this.mainQueryDirectory = map.get("main.query.directory");
			this.pipeline = map.get("pipeline");
			this.lambda = Double.parseDouble(map.get("lambda"));
			this.outputFile = map.get("output.file");

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void createTheRecapFileForAllLambdas() {
		double[] lambdas = {0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9};
		for(double lambda : lambdas) {
			this.lambda = lambda;
			this.createTheRecapFile();
		}
	}
	
	/** Let's do this, man.
	 * <p>
	 * Creates a csv file with the recap of the most important information
	 * of the current pipeline.
	 * 
	 * */
	public void createTheRecapFile() {

		List<Double> dcgValues = new ArrayList<Double>();
		List<Double> precValues = new ArrayList<Double>();
		String metadata = "";
		double mean = 0;
		double precMean = 0;
		for(int i = 1; i <=50; ++i) {
			metadata += (i + ", ");
			//open the directory with the query
			String dirPath = this.mainQueryDirectory + "/" + i;
			File dir = new File(dirPath);
			double dcg = 0;
			double precision = 0;
			
			//reach for the information
			double[] vals = this.dealWithGenericQuery(dir, this.pipeline);
			precision = vals[0];
			dcg = vals[1];
			
			//add it to the list
			dcgValues.add(dcg);
			precValues.add(precision);
			mean += (dcg);
			precMean += precision;
		}
		mean /= 50;
		precMean /= 50;

		Path outPath = Paths.get(this.outputFile + "_" + this.lambda + ".txt");
		try(BufferedWriter writer = Files.newBufferedWriter(outPath, UsefulConstants.CHARSET_ENCODING)) {
			//write first line with metadata
			writer.write("#" + this.pipeline );
			writer.newLine();
			writer.write("queryNo,tbdcg,precision");
			writer.newLine();
			//write the rest
			for(int i = 0; i < 50; ++i) {
				writer.write("query " + (i+1) + "," + dcgValues.get(i)
				+ "," + precValues.get(i));
				writer.newLine();
			}
			writer.write("mean,"+mean + "," + precMean);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private double[] dealWithGenericQuery(File dir, String method) {
		//get the path of the info file
		String infoPath = "";
		if(method.equals("YOSI"))
			infoPath = dir.getAbsolutePath() + "/answer/evaluation/info_" + this.lambda + ".txt";
		else if(method.equals("YOSI_EXT"))
			infoPath = dir.getAbsolutePath() + "/answer/extended_evaluation/info_" + this.lambda + ".txt";
		else if(method.equals("BLANCO"))
			infoPath = dir.getAbsolutePath() + "/evaluation/info_" + this.lambda + ".txt";
		else if(method.equals("TSA+IR"))
			infoPath = dir.getAbsolutePath() + "/IR/evaluation/info_" + this.lambda + ".txt";
		else if(method.equals("TSA+YOSI"))
			infoPath = dir.getAbsolutePath() + "/YOSI/evaluation/info_" + this.lambda + ".txt";
		else if(method.equals("TSA+BLANCO"))
			infoPath = dir.getAbsolutePath() + "/BLANCO/evaluation/info_" + this.lambda + ".txt";
		else if(method.equals("TSA+IR-U"))
			infoPath = dir.getAbsolutePath() + "/IR/ultimate_evaluation/info_" + this.lambda + ".txt";
		else if(method.equals("TSA+plainIR"))
			infoPath = dir.getAbsolutePath() + "/IR/plain_evaluation/info_" + this.lambda + ".txt";
		else if(method.equals("TSA+PRUNING"))
			infoPath = dir.getAbsolutePath() + "/PRUNING/evaluation/info_" + this.lambda + ".txt";
		else if(method.equals("TSA+PRUNINGfly"))
			infoPath = dir.getAbsolutePath() + "/PRUNING/fly_evaluation/info_" + this.lambda + ".txt";
		
		//open the file
		Path inPath = Paths.get(infoPath);
		try(BufferedReader reader = Files.newBufferedReader(inPath, UsefulConstants.CHARSET_ENCODING)) {
			//take the second line
			reader.readLine();
			String line = reader.readLine();
			String[] parts = line.split(",");

			int den = Integer.parseInt(parts[0]);
			int num = Integer.parseInt(parts[1]);
			double precision = (double) num/den;

			double dcg = Double.parseDouble(parts[2]);

			return new double[] {precision, dcg};
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	


	

	/** Test main*/
	public static void main(String[] args) {
		RecapFileCreator execution = new RecapFileCreator();
//		execution.createTheRecapFile();
		execution.createTheRecapFileForAllLambdas();
	}

}
