package it.unipd.dei.ims.rum.assessment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Map;

import org.openrdf.model.Model;
import org.openrdf.model.Statement;
import org.openrdf.model.impl.TreeModel;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.RDFParseException;
import org.openrdf.rio.RDFParser;
import org.openrdf.rio.Rio;
import org.openrdf.rio.helpers.StatementCollector;

import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;

/** Executes the assessment for a query
 * provided one file of results.
 * */
public class KeywordSearchAssessor {

	/** The graph ground truth,
	 * the answer to the structured query
	 * */
	private Collection<Statement> groundTruth;

	private Map<String, String> map; 

	/** Path to the result file
	 * */
	private String resultFilePath;

	/** Path of the directory where we want to save the .eval file
	 * */
	private String outputEvaluationDirectoryPath;

	/** Directory where to find the graphs used to answer.
	 * */
	private String answerGraphsDirectory;

	/** true if we are dealing with more than 1 query.
	 * */
	private boolean multipleQueries;

	/** set to true if the answer graphs are organized in
	 * sub-directories.
	 * */
	private boolean folding;

	/** threshold to be used in the computation of the
	 * signal to noise ration in order to understand if a graph is relevant or not.
	 * */
	private double lambda;

	/** threshold to be used to begin to discount the triple based
	 * discounted gain.
	 * */
	private int thresholdRank;

	public KeywordSearchAssessor() {
		try {
			this.setup();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RDFParseException e) {
			e.printStackTrace();
		} catch (RDFHandlerException e) {
			e.printStackTrace();
		} 
	}

	private void setup() throws IOException, RDFParseException, RDFHandlerException {
		System.out.print("setup of the KeywordSearchAssessor");
		this.map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/assessment.properties");

		//take in the ground truth
		String groundTruthPath = map.get("ground.truth.path");
		this.readGroundTruth(groundTruthPath);

		//take the result file path
		this.resultFilePath = map.get("result.file.path");

		//where to read the answers in order to confront them
		this.answerGraphsDirectory = map.get("answer.graph.directory");

		//take the file where we want to save
		this.outputEvaluationDirectoryPath = map.get("output.evaluation.directory.path");
		File f = new File(this.outputEvaluationDirectoryPath);
		if(!f.exists()) {
			f.mkdirs();
		}

		this.multipleQueries = Boolean.parseBoolean(map.get("multiple.queries"));
		this.folding = Boolean.parseBoolean(map.get("folding"));

		this.lambda = Double.parseDouble(map.get("lambda"));
		this.thresholdRank = Integer.parseInt(map.get("threshold.rank"));

	}



	private void readGroundTruth(String groundTruthPath) {
		System.out.println("reading the ground truth...");
		InputStream inputStream;
		try {
			inputStream = new FileInputStream(new File(groundTruthPath));
			//prepare a collector to contain the triples
			StatementCollector collector = new StatementCollector();
			RDFParser rdfParser = Rio.createParser(RDFFormat.TURTLE);
			//link the collector to the parser
			rdfParser.setRDFHandler(collector);
			//parse the file
			rdfParser.parse(inputStream, "");
			//now get the statements composing the graph
			groundTruth = collector.getStatements();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (RDFParseException e) {
			e.printStackTrace();
		} catch (RDFHandlerException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("ground truth of file " + groundTruthPath + " read");
	}

	/** The method executes the assessment of the .res file with the ground truth*/
	public void executeAssessment() {
		//XXX
		String poolPath = this.outputEvaluationDirectoryPath + "/pool_" + lambda + ".txt";

		//setup the assessor
		RunAssessor runAssessor = new RunAssessor();
		Model groundTruthCopy = new TreeModel(groundTruth);
		runAssessor.setGroundTruth(groundTruthCopy);
		runAssessor.setLambda(lambda);
		runAssessor.setLimitRank(thresholdRank);

		runAssessor.assessRunWithTBDCG(this.answerGraphsDirectory, this.resultFilePath, poolPath, 2048, folding);
		groundTruthCopy.clear();
		//XXX old method of evaluation
		//		RunAssessor.assessRun(groundTruth, 2048, this.answerGraphsDirectory, this.resultFilePath, writingFilePath, poolPath, folding);
	}

	/** Executes the assessment for all the queries of a pipeline given 
	 * our File System structure*/
	public void executeAssessmentMultipleTimes() {
		for(int counter = 1; counter <= 50; counter++) {
			this.executeAssessmentForQueryNumber(counter);
		}
	}
	
	/** Executes the assessments for the algorithm selected 
	 * in the properties file with all lambdas from 0 to 0.9 with
	 * step 0.1 and on all the queries from 1 to 50.
	 * Because automation is the way man
	 * */
	public void executeAssessmentMultipleTimesWithAllLambdas() {
		double[] lambdas = {0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9};
		for(double lambda : lambdas) {
			this.executeAssessmentForLambda(lambda);
		}
	}
	
	public void executeAssessmentForLambda(double lambda) {
		this.lambda = lambda;
		for(int counter = 1; counter <= 50; counter++) {
			this.executeAssessmentForQueryNumber(counter);
		}
	}

	/** In order to estimate a couple of parameters, this method executes
	 * the evaluation for multiple queries, with every query
	 * different candidate collections generated from the merger algorithm.
	 * */
	public void executeAssessmentMultipleTimesForMultipleExecutions() {
		//XXX
		for(int counter = 51; counter <= 100; counter++) {
			this.executeAssessmentForMultipleExecutionsForQueryNumber(counter);
		}
	}

	public void executeAssessmentForQueryNumber(int counter) {
		//get the required informations
		this.setupForQueryNumber(counter);
		this.executeAssessment();
	}

	/** Execute the assessment multiple times for all the collections 
	 * that has been created for the query.
	 * <p>
	 * This method has been created to evaluate the results of multiple execution of
	 * the pipeline TSA+IR with different parameters as
	 * overlapping percentage and lookahead. 
	 * */
	private void executeAssessmentForMultipleExecutionsForQueryNumber(int queryNo) {
		
		this.setupForMultipleExecutionsForQueryNumber(queryNo);
		
		//for this query, get the directory of collections
		String collectionsDirectoryPath = map.get("main.pipeline.query.directory") + "/" 
				+ queryNo + "/collections";
		//list of all the directories
		File f = new File(collectionsDirectoryPath);
		File[] files = f.listFiles();
		for(File colDir : files) {
			if(colDir.isDirectory()) {
				//in this way avoid .DS_Store and what not
				
				//by my own convention, the collections directories are called
				//collection_overlapping%_lookadeag
				String[] names = colDir.getName().split("_");
				String overlappingPercentage = names[1];
				String lookAhead = names[2];
				
				//take the path of the run file
				//XXX
				this.resultFilePath = colDir.getAbsolutePath() + "/IR/run/ultimate_run.txt" ;
				
				//take the directory where the answer graphs are stored
				this.answerGraphsDirectory = colDir.getAbsolutePath() + "/graphs";
				
				//take the directory where to write the output files
				this.outputEvaluationDirectoryPath = colDir.getAbsolutePath() + "/IR/evaluation";

				//build the directory if necessary
				File gigi = new File(this.outputEvaluationDirectoryPath);
				if(!gigi.exists()) {
					gigi.mkdirs();
				}
				
				//now we can do the assessment
				this.executeAssessment();
			}
		}
	}

	private void setupForMultipleExecutionsForQueryNumber(int queryNo) {
		//take the path of the ground truth
		String groundTruthPath = map.get("ground.truths.directory") + "/" + queryNo + ".txt";
		//take in the ground truth
		this.readGroundTruth(groundTruthPath);

	}

	private void setupForQueryNumber(int counter) {
		//take the path of the ground truth
		String groundTruthPath = map.get("ground.truths.directory") + "/" + counter + ".txt";
		//take in the ground truth
		this.readGroundTruth(groundTruthPath);

		//take in the file with the ranking of the graphs/documents
		this.resultFilePath = map.get("main.pipeline.query.directory") + "/" + counter + map.get("result.file.path.add.on") ;

		//take the directory where the answer graphs are stored
		this.answerGraphsDirectory = map.get("main.pipeline.query.directory") + "/" + counter + map.get("answer.graphs.directory.path.add.on");
		//these 4 lines are to deal with the case of plain IR, which uses all the clusters
		String plain = map.get("plain");
		if(plain!=null) {
			this.answerGraphsDirectory = map.get("answer.graphs.directory.path.add.on");
		}

		//take the directory where to write the output files
		this.outputEvaluationDirectoryPath = map.get("main.pipeline.query.directory") + "/" + counter + map.get("evaluation.directory.path.add.on");

		//build the directory if necessary
		File f = new File(this.outputEvaluationDirectoryPath);
		if(!f.exists()) {
			f.mkdirs();
		}
	}


	/** Test main.
	 * <p>
	 * 
	 * NB: The blanco function to find the answer graphs is 
	 * <p>
	 * (int) Math.floor((double)docId / step) + 1;
	 * <p>
	 * for everyone else it is:
	 * <p>
	 * (int) Math.ceil((double)docId / step)
	 * <p>
	 * peace.
	 * */
	public static void main(String[] args) {
		KeywordSearchAssessor assessor = new KeywordSearchAssessor();
		if(assessor.multipleQueries==true) {
//			assessor.executeAssessmentForQueryNumber(44);
//			assessor.executeAssessmentMultipleTimesForMultipleExecutions();
//			assessor.executeAssessmentMultipleTimes();
			assessor.executeAssessmentMultipleTimesWithAllLambdas();
		} else
			assessor.executeAssessment();
	}


}
