package it.unipd.dei.ims.rum.indexing;

import java.util.ArrayList;
import java.util.List;

import org.terrier.indexing.Collection;
import org.terrier.indexing.SimpleXMLCollection;
import org.terrier.structures.Index;
import org.terrier.structures.IndexOnDisk;
import org.terrier.structures.indexing.Indexer;
import org.terrier.structures.indexing.classical.BasicIndexer;

/** This class provides useful methods to index a set of common documents.
 * They are useful wrappers.
 * <p>
 * It uses Terrier to compute the indexing.
 * <p>
 * @param terrier_home
 * */
@Deprecated
public class RumIndexer {
	
	/** Path to the terriere home, the directory of Terrier which we want to use*/
	private String terrier_home;
	/** Path to the terrier/etc directory, the directory containing the file terrier.properties which
	 * describes the behaviour of Terrier.*/
	private String terrier_etc;
	
	/** Directory containing the files we want to index*/
	private String mainDirectory;
	
	/** String containing the path where we want to save the index.*/
	private String indexPath;

	/** This methods creates an index with the documents contained in the provided directory.
	 * 
	 * */
	public void rumIndex () {
		System.setProperty("terrier.home", terrier_home);
		//in the /etc directory it is present the terrier.properties file. Very useful, indeed
		System.setProperty("terrier.etc", terrier_etc);
		
		//get the list of files to index
//		List<String> files = PathUsefulMethods.getListOfFiles(mainDirectory);
		
		List<String> files = new ArrayList<String>();
		files.add("/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/clusters/xml/0.xml");
		
		//terrier collection of xml files
		Collection collection = new SimpleXMLCollection(files);
		
		//index the collection
		Indexer indexer = new BasicIndexer(indexPath, "data");
		indexer.index(new Collection[]{ collection });
		
		//open the new index
		Index index = IndexOnDisk.createIndex(indexPath, "data");
		System.out.println("We have indexed " + index.getCollectionStatistics().getNumberOfDocuments() + " documents");
		
		System.out.print("Indexing has been completed in directory " + indexPath);
	}
	
	//###########
	
	

	//##########
	
	public String getTerrier_home() {
		return terrier_home;
	}

	public void setTerrier_home(String terrier_home) {
		this.terrier_home = terrier_home;
	}

	public String getTerrier_etc() {
		return terrier_etc;
	}

	public void setTerrier_etc(String terrier_etc) {
		this.terrier_etc = terrier_etc;
	}

	public String getMainDirectory() {
		return mainDirectory;
	}

	public void setMainDirectory(String mainDirectory) {
		this.mainDirectory = mainDirectory;
	}

	public String getIndexPath() {
		return indexPath;
	}

	public void setIndexPath(String indexPath) {
		this.indexPath = indexPath;
	}
}
