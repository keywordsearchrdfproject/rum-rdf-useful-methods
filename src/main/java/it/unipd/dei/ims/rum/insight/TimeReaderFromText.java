package it.unipd.dei.ims.rum.insight;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class TimeReaderFromText {

	private String filename;
	
	
	public void readTimesFromFile() {
		Path inPath = Paths.get(this.filename);
		double totalTime = 0;
		int counter = 0;
		try(BufferedReader reader = Files.newBufferedReader(inPath)) {
			String line = "";
			while((line = reader.readLine()) != null) {
//				boolean b = line.contains("complexively  required");
//				boolean b = line.contains("the total time required to compute");
				boolean b = line.contains("ended in");
				if(b) {
					counter++;
					
					String[] parts = line.split(":");
					String tTime = parts[1];
					String time = tTime.split(" ")[1];
					String measure = tTime.split(" ")[2];
					System.out.println(tTime);
					if(measure.equals("min")) {
						double thisScore = Double.parseDouble(time);
						totalTime += thisScore;
					} else if(measure.equals("s")) {
						double thisScore = (double) Double.parseDouble(time) / 60;
						totalTime += thisScore;
					}
					
					
				}
 			}
			double meanTime = (double) totalTime / (counter);
			System.out.println("counter: " + counter + "\n totalTime in minutes: " + totalTime + "\n average in minutes : " + meanTime);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		TimeReaderFromText execution = new TimeReaderFromText();
		execution.setFilename("/Users/dennisdosso/Desktop/eclipse_output/new execution from scratch/IMDB/ir_queries.txt");
		execution.readTimesFromFile();
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}
}
