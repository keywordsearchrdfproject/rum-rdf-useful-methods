package it.unipd.dei.ims.rum.main;

import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;

import it.unipd.dei.ims.rum.convertion.IMDBFromTSVFileToRDFRepositoryConverter;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;

public class RumFromTSVFileTORDFRepositoryConverterExecutable {

	public static void main(String[] args) throws IOException {
		
		Map<String, String> propertyMap = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/general.properties");
		String blazegraphDatabase = propertyMap.get("blazegraph.database");
		Map<String, String> pathMap = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/files_to_convert.properties");
		
		
		//XXX DEBUG
		blazegraphDatabase = "/Users/dennisdosso/Documents/RDF_DATASETS/IMDB/dataset/dataset.jnl";
		
		for(Entry<String, String> entry : pathMap.entrySet()) {
			String key = entry.getKey();
			System.out.println("now importing file "+ key);
			String inputFile = entry.getValue();
			IMDBFromTSVFileToRDFRepositoryConverter.convertionFromTSVIntoRDFDatabaseByChuncks(inputFile, blazegraphDatabase);
		}
		System.out.print("done");
		
		
	}
}
