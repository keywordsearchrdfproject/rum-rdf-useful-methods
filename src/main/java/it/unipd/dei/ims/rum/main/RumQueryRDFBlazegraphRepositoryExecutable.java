package it.unipd.dei.ims.rum.main;

import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;

import it.unipd.dei.ims.rum.blazegraph.query.RumQueryRDFBlazegraphRepository;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;

/** Computes a series of SPARQL queries.
 * */
public class RumQueryRDFBlazegraphRepositoryExecutable {

	public static void main(String[] args) throws IOException {
		//take all the necessary informations 
		Map<String, String> propMap = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/query.properties");
		String database = propMap.get("database.to.query");
		String prefixString = propMap.get("sparql.prefix");
		String out = propMap.get("output.directory");
		
		Map<String, String> queryMap = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/query_list.properties");
		
		//DEBUG
//		database = "/Users/dennisdosso/Documents/RDF_DATASETS/linkedMDB/rdf_database/dataset.jnl";
		
		for(Entry<String, String> entry : queryMap.entrySet()) {
			String key = entry.getKey();
			String out2 = out + "/" + key + ".ttl";
			String query = entry.getValue();
			RumQueryRDFBlazegraphRepository.queryRDFDatabase(database, query, prefixString,  out2);; 
		}
	}
}
