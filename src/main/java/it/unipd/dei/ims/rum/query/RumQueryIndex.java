/**
 * 
 */
package it.unipd.dei.ims.rum.query;

import java.io.IOException;
import java.util.Map;

import org.terrier.matching.ResultSet;
import org.terrier.querying.Manager;
import org.terrier.querying.SearchRequest;
import org.terrier.structures.Index;
import org.terrier.structures.IndexOnDisk;

import it.unipd.dei.ims.rum.indexing.RumTRECIndexer;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;

/**
 * Class to query an index with Terrier.
 * @author dennisdosso
 *
 */
public class RumQueryIndex {

	/** Path to the terriere home, the directory of Terrier which we want to use*/
	private String terrier_home;
	/** Path to the terrier/etc directory, the directory containing the file terrier.properties which
	 * describes the behaviour of Terrier.*/
	private String terrier_etc;

	/** String containing the path where we want to save the index.*/
	private String indexPath;

	//###############

	/**This method executes a query and writes the results on a file.
	 * */
	public void executeQuery () {
		System.setProperty("terrier.home", terrier_home);
		//in the /etc directory it is present the terrier.properties file. Very useful, indeed
		System.setProperty("terrier.etc", terrier_etc);

		//open the new index
		Index index = IndexOnDisk.createIndex(indexPath, "data");
		System.out.println("We have indexed " + index.getCollectionStatistics().getNumberOfDocuments() + " documents");

		//prepare the query
		Manager queryingManager = new Manager(index);
		SearchRequest srq = queryingManager.newSearchRequestFromQuery("TEST");

		//choose the ranking model
		srq.addMatchingModel("Matching", "BM25");

		queryingManager.runSearchRequest(srq);

		ResultSet results = srq.getResultSet();
		System.out.println(results.getExactResultSize()+" documents were scored");
		System.out.println("The top "+results.getResultSize()+" of those documents were returned");

		System.out.println("Document Ranking");
		try {
			for (int i =0; i< results.getResultSize(); i++) {
				int docid = results.getDocids()[i];

				String docno;
				docno = index.getMetaIndex().getItem("docno", docid);
				double score = results.getScores()[i];
				System.out.println("   Rank "+i+" docno: "+ docno + " "+" "+score);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	/**
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		RumQueryIndex idx = new RumQueryIndex();
		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/indexing.properties");

		idx.setTerrier_home(map.get("terrier.home"));
		idx.setTerrier_etc(map.get("terrier.etc"));
		String indexP = "/Users/dennisdosso/Documents/terrier/terrier-core-4.2/var/index";
		idx.setIndexPath(indexP);

		//index
		idx.executeQuery();
	}



	public String getTerrier_home() {
		return terrier_home;
	}



	public void setTerrier_home(String terrier_home) {
		this.terrier_home = terrier_home;
	}



	public String getTerrier_etc() {
		return terrier_etc;
	}



	public void setTerrier_etc(String terrier_etc) {
		this.terrier_etc = terrier_etc;
	}



	public String getIndexPath() {
		return indexPath;
	}



	public void setIndexPath(String indexPath) {
		this.indexPath = indexPath;
	}

}
