package it.unipd.dei.ims.rum.consolidated;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

import org.openrdf.model.Statement;
import org.openrdf.query.GraphQuery;
import org.openrdf.query.GraphQueryResult;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.RDFWriter;
import org.openrdf.rio.Rio;

import it.unipd.dei.ims.rum.utilities.BlazegraphUsefulMethods;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;

/** This class presents methods
 * to query a database RDF with construct queries and write them in documents
 * .
 * */

public class ConstructQueryBlazegraphToolkit {

	/** Path of the RDF database to interrogate*/
	private String rdfDatabasePath; 

	/** SPARQL query. You can write it without the use of prefixes.*/
	private String sparqlQuery; 

	/** String with the prefix you want to use with
	 * the SPARQL query.
	 * */
	private String prefixString;

	/** String with the output .txt file where to write the results of the query
	 * */
	private String outputFilePath;


	public ConstructQueryBlazegraphToolkit () {

	}

	/** Method to test the presence of triples inside a dataset RDF.
	 * */
	public static void queryRDFDatabaseTest(String databasePath) {
		//open the connection to the database

		Repository repo = BlazegraphUsefulMethods.getRepositoryFromPath(databasePath);
		try {
			repo.initialize();
			RepositoryConnection cxn = BlazegraphUsefulMethods.getRepositoryConnection(repo);

			//query to create a graph
			GraphQuery graphQuery = cxn.prepareGraphQuery(QueryLanguage.SPARQL, "construct where { ?s ?p ?o . } LIMIT 200");
			GraphQueryResult result = graphQuery.evaluate();
			while(result.hasNext()) {
				Statement t = result.next();
				System.out.println(t.getSubject() + " " + t.getPredicate() + " " + t.getObject());
			}

		} catch (RepositoryException e) {
			e.printStackTrace();
		} catch (MalformedQueryException e) {
			e.printStackTrace();
		} catch (QueryEvaluationException e) {
			e.printStackTrace();
		}
	}

	/** This methods performs a CONSTRUCT query over the database
	 * 
	 * @param databasePath the path of the Blazegraph file .jnl to be interrogated
	 * @param query the SPARQL Construct query to be evaluated
	 * @param prefixString string containing all the prefixes used in the query
	 * */
	public static void queryRDFDatabase(String databasePath, String query, String prefixString, String out) {

		//open the connection to the database
		Repository repo = BlazegraphUsefulMethods.getRepositoryFromPath(databasePath);
		try {
			repo.initialize();
			RepositoryConnection cxn = BlazegraphUsefulMethods.getRepositoryConnection(repo);

			//query to create a graph
			GraphQuery graphQuery = cxn.prepareGraphQuery(QueryLanguage.SPARQL, prefixString + " " + query);
			//evaluate the query
			GraphQueryResult result = graphQuery.evaluate();

			//write the file in output
			File f = new File(out);
			OutputStream output = new FileOutputStream(f);
			RDFWriter writer = Rio.createWriter(RDFFormat.TURTLE, output);

			writer.startRDF();
			while(result.hasNext()) {
				Statement t = result.next();
				writer.handleStatement(t);
				System.out.println(t.getSubject() + " " + t.getPredicate() + " " + t.getObject());
			}
			writer.endRDF();
			output.close();

		} catch (RepositoryException e) {
			e.printStackTrace();
		} catch (MalformedQueryException e) {
			e.printStackTrace();
		} catch (QueryEvaluationException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RDFHandlerException e) {
			e.printStackTrace();
		}
	}

	public static void queryRDFDatabasePrintOnlyOnSystemOut(String databasePath, String query, String prefixString, String out) {

		//open the connection to the database
		Repository repo = BlazegraphUsefulMethods.getRepositoryFromPath(databasePath);
		try {
			repo.initialize();
			RepositoryConnection cxn = BlazegraphUsefulMethods.getRepositoryConnection(repo);

			//query to create a graph
			GraphQuery graphQuery = cxn.prepareGraphQuery(QueryLanguage.SPARQL, prefixString + " " + query);
			//evaluate the query
			GraphQueryResult result = graphQuery.evaluate();


			while(result.hasNext()) {
				Statement t = result.next();
				System.out.println(t.getSubject() + " " + t.getPredicate() + " " + t.getObject());
			}

		} catch (RepositoryException e) {
			e.printStackTrace();
		} catch (MalformedQueryException e) {
			e.printStackTrace();
		} catch (QueryEvaluationException e) {
			e.printStackTrace();
		}
	}


	/** Test main
	 * */
	public static void main(String[] args) throws IOException {
		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/query.properties");
		String prefixString = map.get("sparql.prefix");
		String databasePath = "/Users/dennisdosso/Documents/RDF_DATASETS/IMDB/dataset/dataset.jnl";
		int number = 0;
		String out = "/Users/dennisdosso/Desktop/" + number + ".txt";


		String query = "CONSTRUCT WHERE { "
				+ "?actor_id imdb:primary_profession imdb_primary_profession:actor ;"
				+ "imdb:primary_name \"Sean Connery\" ;"
				+ "imdb:known_for_titles ?title_id ."
				+ "?title_id imdb:primary_title ?title ;"
				+ "imdb:title_type imdb_title_type:movie ;"
				+ "imdb:genres ?genre . "
				+ "} LIMIT 8000";

		ConstructQueryBlazegraphToolkit.queryRDFDatabase(databasePath, query, prefixString, out);
	}

	public String getRdfDatabasePath() {
		return rdfDatabasePath;
	}

	public void setRdfDatabasePath(String rdfDatabasePath) {
		this.rdfDatabasePath = rdfDatabasePath;
	}

	public String getSparqlQuery() {
		return sparqlQuery;
	}

	public void setSparqlQuery(String sparqlQuery) {
		this.sparqlQuery = sparqlQuery;
	}

	public String getPrefixString() {
		return prefixString;
	}

	public void setPrefixString(String prefixString) {
		this.prefixString = prefixString;
	}

	public String getOutputFilePath() {
		return outputFilePath;
	}

	public void setOutputFilePath(String outputFilePath) {
		this.outputFilePath = outputFilePath;
	}

}
