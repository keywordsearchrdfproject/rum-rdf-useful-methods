package it.unipd.dei.ims.rum.clustering.test;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.openrdf.model.Value;
import org.openrdf.query.BindingSet;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;

import com.bigdata.journal.Options;
import com.bigdata.rdf.sail.BigdataSail;
import com.bigdata.rdf.sail.BigdataSailRepository;

import it.unipd.dei.ims.rum.utilities.BlazegraphUsefulMethods;
import it.unipd.dei.ims.rum.utilities.MapsUsefulMethods;
import it.unipd.dei.ims.rum.utilities.SQLUtilities;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;

public class BlazegraphStatisticsWithDatabaseTest {
	/**The cardinality of E, |E|*/
	private int edgeSetCardinality;
	/**Keeps the record c(l, E) for every label l*/
	private Map<String, Integer> labelCounterMap;
	/**Map containing the numerator of the ACD(l) for each label l appearing in the graph*/
	private Map<String, Integer> labelAverageDegreeMap;
	/**Contains the ACD for each label l*/
	private Map<String, Double> averageCombinedDegreeMap;
	/**Stores the beta score for each label*/
	private Map<String, Double> betaScoreMap;
	/**Contains the harmonic mean between the ACD and the frequency for each label l*/
	private Map<String, Double> hMeanMap;

	/**Keeps track of the in degree of the various nodes in the RDF model*/
	private Map<String, Integer> inDegreeMap;
	/**Keeps track of the out degree of the various nodes in the RDF model*/
	private Map<String, Integer> outDegreeMap;

	/**the two beta parameters composing the beta-score function*/ 
	private double beta1 = 1;
	private double beta2 = 1;

	//constructors
	public BlazegraphStatisticsWithDatabaseTest () {
		this.edgeSetCardinality = 0;
		this.labelCounterMap = new HashMap<String, Integer>();
		this.labelAverageDegreeMap = new HashMap<String, Integer>();
		this.inDegreeMap = new HashMap<String, Integer>();
		this.outDegreeMap = new HashMap<String, Integer>();
		this.betaScoreMap = new HashMap<String, Double>();
		this.averageCombinedDegreeMap = new HashMap<String, Double>();
		this.hMeanMap = new HashMap<String, Double>();
	}

	/**Calculates the statistics of the Model (RDF graph) passed
	 * as parameter. 
	 * <p>These statistics includes:
	 * <ul>
	 * <li>|E|, cardinality of the edge set</li>
	 * <li>In degree of the nodes</li>
	 * <li>Out degree of the nodes</li>
	 * <li>Frequency of each label</li>
	 * <li>Average Combined Degree (AVD) of each label</li>
	 * <li>The beta-score of each label</li>
	 * </ul>
	 * </p>
	 * 
	 * @param dataset The path of the dataset to be read in order to compute its statistics
	 * @throws RepositoryException 
	 * */
	public BlazegraphStatisticsWithDatabaseTest getGraphStatistics(String dataset, String rdbDatabase) throws RepositoryException {

		System.out.println("starting making statistics...");

		////info to access the rdb database
		String connectionString = "jdbc:postgresql://localhost:5432/disgenet?user=postgres&password=Ulisse92";
		rdbDatabase = connectionString;
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		Connection connection = null;
		
		////

		Properties props = new Properties();
		props.put(Options.BUFFER_MODE, "DiskRW");
		props.put(Options.FILE, dataset);

		final BigdataSail sail = new BigdataSail(props); // instantiate a sail
		final Repository repo = new BigdataSailRepository(sail); // create a Sesame repository
		//initialize the repository

		try {
			//open the connection to the database (remember to shut it off at the end)
			repo.initialize();
			RepositoryConnection cxn = BlazegraphUsefulMethods.getRepositoryConnection(repo);

			////
			connection = DriverManager.getConnection(connectionString);
			////



			TupleQueryResult iterator = BlazegraphUsefulMethods.getIterator(cxn);
			System.out.println("got the iterator");


			//monitor thread
			final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
			scheduler.scheduleAtFixedRate(new MonitorStatisticsThread(), 10, 10, TimeUnit.SECONDS);

			int lineCounter = 0;
			while(iterator.hasNext()) {
				//read the next triple
				BindingSet bs = iterator.next();
				this.edgeSetCardinality++;
				lineCounter++;

				//take subject, predicate and object
				Value subject = bs.getValue("s");
				Value predicate = bs.getValue("p");
				Value object = bs.getValue("o");

				//count +1 for the number of times we have seen the predicate
				String predicateString = predicate.toString();
				Integer value = this.labelCounterMap.get(predicateString);
				if(value == null) {
					//first time we see this label
					this.labelCounterMap.put(predicateString, 1);
					///
				}
				else {
					//update the value
					this.labelCounterMap.put(predicateString, value + 1);
				}

				if(lineCounter > 1000000) {
					lineCounter = 0;
					System.out.println("Updating the database");
					SQLUtilities.updateLabelFrequencyIntoDatabaseFromMap(connection, labelCounterMap);
				}

				//update out degree and in degree of subject and object
				//respectively
				//				value = outDegreeMap.get(subject.toString());
				//				if(value == null) {
				//					outDegreeMap.put(subject.toString(), 1);
				//				}
				//				else {
				//					outDegreeMap.put(subject.toString(), value + 1);
				//				}

				//(in degree now)
				//				value = inDegreeMap.get(object.toString());
				//				if(value == null) {
				//					//first time we see this object
				//					inDegreeMap.put(object.toString(), 1);
				//				}
				//				else {
				//					//update the in degree
				//					inDegreeMap.put(object.toString(), value + 1);
				//				}

				//				//get the degree of this triple with this predicate
				////				int predicateDegree = BlazegraphUtilities.getDegreeOfATriple(cxn, bs);
				//				int predicateDegree = 1;
				//				//put it in the map
				//				value = this.labelAverageDegreeMap.get(predicateString);
				//				if(value == null) {
				//					//first time we see this label
				//					this.labelAverageDegreeMap.put(predicateString, predicateDegree);
				//				} else {
				//					//update the count
				//					value = value + predicateDegree;
				//					this.labelAverageDegreeMap.put(predicateString, value);
				//				}
				//				
			}
			///
			//clean up
			System.out.println("number of labels: " + labelCounterMap.size());
			System.out.println("number of nodes with out edges: " + outDegreeMap.size());
			System.out.println("number of nodes with in edges: " + inDegreeMap.size());			
			cxn.close();
			System.out.println("committing to the RDB database...");
//			connection.commit();
			scheduler.shutdownNow();
			iterator.close();

		} catch (RepositoryException e) {
			System.err.println("error with repository " + dataset);
			e.printStackTrace();
		} catch (QueryEvaluationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			repo.shutDown();
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		System.out.println("all edges read, done");

		//creation of the scores for the labels
		for (String label : labelCounterMap.keySet()) {//for each label
			//compute the score
			double firstScore = ((double)labelCounterMap.get(label) / (double)edgeSetCardinality);
			this.hMeanMap.put(label, firstScore);
			//the score has been simplified keeping in count the objective difficulty of the implementation
			//when dealing with graph with more than 2.000.000 triples the computation
			//of the originally planned score became unfeasible
		}

		//		printTheStatistics(this);
		return this;

	}

	/**Method to print the statistics at the end of the first run. Then you can read
	 * them instead of computing them until the database stays the same.
	 * */
	public void printTheStatistics(BlazegraphStatisticsWithDatabaseTest stat) {
		System.out.println("printing the statistics...");
		Map<String, String> map = PropertiesUsefulMethods.getProperties();

		String labelCounterMapPath = map.get("clustering.support.directory") + "statistics.csv";
		Path out = Paths.get(labelCounterMapPath);

		try(BufferedWriter writer = Files.newBufferedWriter(out)) {
			//|E|
			writer.write("edge set cardinality," + stat.edgeSetCardinality);
			writer.newLine();
			writer.newLine();

			writer.write("label counter map");
			writer.newLine();
			for(Entry<String, Integer> entry : stat.labelCounterMap.entrySet()) {
				writer.write(entry.getKey() + "," +entry.getValue());
				writer.newLine();
			}
			writer.newLine();

			writer.write("score map");
			writer.newLine();
			for(Entry<String, Double> entry : stat.hMeanMap.entrySet()) {
				writer.write(entry.getKey() + "," +entry.getValue());
				writer.newLine();
			}
			writer.newLine();

			writer.write("in degree map");
			writer.newLine();
			for(Entry<String, Integer> entry : stat.inDegreeMap.entrySet()) {
				writer.write(entry.getKey() + "," +entry.getValue());
				writer.newLine();
			}
			writer.newLine();

			writer.write("out degree map");
			writer.newLine();
			for(Entry<String, Integer> entry : stat.outDegreeMap.entrySet()) {
				writer.write(entry.getKey() + "," +entry.getValue());
				writer.newLine();
			}

			writer.close();
			System.out.println("done");

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**Returns the top k elements of the connectivity list computed
	 * in the getStatistics method.
	 * 
	 * @param k the number of elements from the top to be taken.
	 * @param measure a String describing what measure to use to rank the labels. Allowed 
	 * parameters here are: beta (for the beta score) and f-measure (for the f-measure scoring).*/
	public List<String> getKConnectivityList(int k, String measure) {


		int counter = 0;
		List<String> connectivityList = new ArrayList<String>();
		Map<String, Double> scoreMap = null;

		if(measure.equals("beta")) {
			this.betaScoreMap = MapsUsefulMethods.sortByValue(this.betaScoreMap);
			scoreMap = this.betaScoreMap;
		}
		else if(measure.equals("harmonic-mean")) {
			this.hMeanMap = MapsUsefulMethods.sortByValue(this.hMeanMap);
			scoreMap = this.hMeanMap;
		}

		for( String label : scoreMap.keySet()) {
			//for each label (we have ordered them)
			connectivityList.add(label);
			counter++;
			if(counter >= k)//if we reach the limit k before, we stop
				break;
		}

		printConnectivityList(connectivityList);
		return connectivityList;
	}

	private void printConnectivityList(List<String> connectivityList) {
		Map<String, String> map = PropertiesUsefulMethods.getProperties();

		String labelCounterMapPath = map.get("clustering.support.directory") + "connectivityList.csv";
		Path out = Paths.get(labelCounterMapPath);

		try(BufferedWriter writer = Files.newBufferedWriter(out)) {
			for(String st : connectivityList) {
				writer.write(st + ",");
			}
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private class MonitorStatisticsThread extends Thread {
		public void run() {
			System.out.println("Read " + BlazegraphStatisticsWithDatabaseTest.this.edgeSetCardinality + " edges,"
					+ " read " + BlazegraphStatisticsWithDatabaseTest.this.labelCounterMap.size() + " different labels,"
					+ " read " + BlazegraphStatisticsWithDatabaseTest.this.outDegreeMap.size() + " out nodes,"
					+ " read " + BlazegraphStatisticsWithDatabaseTest.this.inDegreeMap.size() + " in nodes");
		}
	}

	public int getEdgeSetCardinality() {
		return edgeSetCardinality;
	}

	public void setEdgeSetCardinality(int edgeSetCardinality) {
		this.edgeSetCardinality = edgeSetCardinality;
	}

	public Map<String, Integer> getLabelCounterMap() {
		return labelCounterMap;
	}

	public void setLabelCounterMap(Map<String, Integer> labelCounterMap) {
		this.labelCounterMap = labelCounterMap;
	}

	public Map<String, Integer> getLabelAverageDegreeMap() {
		return labelAverageDegreeMap;
	}

	public void setLabelAverageDegreeMap(Map<String, Integer> labelAverageDegreeMap) {
		this.labelAverageDegreeMap = labelAverageDegreeMap;
	}

	public Map<String, Double> getAverageCombinedDegreeMap() {
		return averageCombinedDegreeMap;
	}

	public void setAverageCombinedDegreeMap(Map<String, Double> averageCombinedDegreeMap) {
		this.averageCombinedDegreeMap = averageCombinedDegreeMap;
	}

	public Map<String, Double> getBetaScoreMap() {
		return betaScoreMap;
	}

	public void setBetaScoreMap(Map<String, Double> betaScoreMap) {
		this.betaScoreMap = betaScoreMap;
	}

	public Map<String, Double> gethMeanMap() {
		return hMeanMap;
	}

	public void sethMeanMap(Map<String, Double> hMeanMap) {
		this.hMeanMap = hMeanMap;
	}

	public Map<String, Integer> getInDegreeMap() {
		return inDegreeMap;
	}

	public void setInDegreeMap(Map<String, Integer> inDegreeMap) {
		this.inDegreeMap = inDegreeMap;
	}

	public Map<String, Integer> getOutDegreeMap() {
		return outDegreeMap;
	}

	public void setOutDegreeMap(Map<String, Integer> outDegreeMap) {
		this.outDegreeMap = outDegreeMap;
	}

	public double getBeta1() {
		return beta1;
	}

	public void setBeta1(double beta1) {
		this.beta1 = beta1;
	}

	public double getBeta2() {
		return beta2;
	}

	public void setBeta2(double beta2) {
		this.beta2 = beta2;
	}

}
