package it.unipd.dei.ims.rum.test.lucene;

import java.io.IOException;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;

public class LuceneFirstTest {

	public static void main(String[] args) throws IOException {
		
		
		
		
		
		//new Index
		StandardAnalyzer standardAnalyzer = new StandardAnalyzer();
		Directory directory = new RAMDirectory();
		IndexWriterConfig config = new IndexWriterConfig(standardAnalyzer);
		//create a writer
		IndexWriter writer = new IndexWriter(directory, config);
		Document document = new Document();
		
		//add field to the document
		document.add(new TextField("content", "Hello World", Field.Store.YES));
		//add document to the index
		writer.addDocument(document);
		writer.commit();
		
		//repeat
		document.add(new TextField("content", "Hello Bitches", Field.Store.YES));
		writer.addDocument(document);
		
		writer.close();
		
		IndexReader reader = DirectoryReader.open(directory);
		IndexSearcher searcher = new IndexSearcher(reader);
		
		
		
	}

}
