package it.unipd.dei.ims.rum.relevance;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;

import it.unipd.dei.ims.rum.utilities.PathUsefulMethods;
import it.unipd.dei.ims.rum.utilities.UsefulConstants;

/** Clas which executes the assessment of a set of answer graphs. 
 * 
 * It uses Jena as graph library.
 * */
@Deprecated
public class BasicRunAssessor {
	
	/**Assess one run.
	 * 
	 * <p>
	 * NB: we suppose the run file to be a standard Terrier file. That is, each line is in the form:
	 * </p>
	 * <p>
	 * query_no Qxy docID rank score model_name
	 * </p>
	 * <p>
	 * Where query_no means query number. xy is the number of the query (when the run is 
	 * composed by more than one query). docID is the id of the document/graph. rank is the rank and it is
	 * meant in ascending order. Score is the score of the document provided by the model. The last
	 * element is the name of the model.
	 * </p>
	 * 
	 * @param groundTruth string with the path of the ground truth graph.
	 * @param graphDirectory directory where are located the graphs.
	 * @param runFile file .res with the ranking of the documents.
	 * @param writingFile where to write the results
	 * */
	public static void assessRun(String groundTruth, String graphDirectory, String runFile, String writingFile) {
		//open the run file
		Path inputPath = Paths.get(runFile);
//		File f = new File(writingFile);
		Path outputPath = Paths.get(writingFile);
		
		//take the list of all the graphs
		System.out.println("Getting all the files..");
		Map<String, String> graphMap = PathUsefulMethods.getPathsAndIDsOfAllFilesInsideDirectoryRecursively(graphDirectory);
		
		//read the ground truth
		Model gt = ModelFactory.createDefaultModel();
		File gtFile = new File(groundTruth);
		InputStream in = null;
		try {
			in = new FileInputStream(gtFile);
			String l = PathUsefulMethods.getTheFormatOfRDFFile(groundTruth);
			gt.read(in, null, l);
			in.close();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//read the run file
		try (BufferedReader reader = Files.newBufferedReader(inputPath, UsefulConstants.CHARSET_ENCODING)) {
			BufferedWriter writer = Files.newBufferedWriter(outputPath, UsefulConstants.CHARSET_ENCODING);
			/*read one line at the time.*/
			String line = "";
			
			while((line = reader.readLine())!= null) {
				//for each graph
				String[] parts = line.split(" ");
				String docId = parts[2];
				String rank = parts[3];
				//use the id to retrieve the rank
				String graphPath = graphMap.get(docId);
				
				if(graphPath == null)
					throw new IllegalArgumentException("ID of a path not found " + docId);
				
				
				//take the graph to assess
				Model m = ModelFactory.createDefaultModel();
				File mFile = new File(graphPath);
				InputStream in2 = null;
				try {
					in2 = new FileInputStream(mFile);
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				}
				String lan = PathUsefulMethods.getTheFormatOfRDFFile(graphPath);
				m.read(in2, null, lan);
				in2.close();
				
				BasicGraphRelevanceAssessor assessor = new BasicGraphRelevanceAssessor(gt);
				GraphAssessment assess = assessor.assess(m, Integer.parseInt(docId), Integer.parseInt(rank));
				
				writer.write(assess.toString());
				writer.newLine();
				writer.flush();
				
				System.out.println(assess);
				
			}
			writer.close();
			
			
		} catch (IOException e) {
			System.out.println("problem reading run file: " + runFile);
			e.printStackTrace();
		}
		
	}

}
