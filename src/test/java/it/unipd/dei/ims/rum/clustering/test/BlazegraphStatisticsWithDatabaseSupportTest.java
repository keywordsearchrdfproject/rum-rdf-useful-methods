package it.unipd.dei.ims.rum.clustering.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.openrdf.model.Value;
import org.openrdf.query.BindingSet;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;

import com.bigdata.journal.Options;
import com.bigdata.rdf.sail.BigdataSail;
import com.bigdata.rdf.sail.BigdataSailRepository;

import it.unipd.dei.ims.rum.utilities.BlazegraphUsefulMethods;
import it.unipd.dei.ims.rum.utilities.MapsUsefulMethods;
import it.unipd.dei.ims.rum.utilities.SQLUtilities;
//XXX current utilized class

/**This class utilize Blazegraph as support library to deal with RDF and JDBC
 * to save information in memory.
 * */
public class BlazegraphStatisticsWithDatabaseSupportTest {
	
	private int edgeSetCardinality;
	
	/**This map keeps track of the frequencies of the labels in the graph.*/
	private Map<String, Integer> labelCounterMap;
	
	/**This map helps keeping track of the in degree of the nodes
	 * */
	private Map<String, Integer> inDegreeMap;
	
	/**This map helps keeping track of the out degree
	 * */
	private Map<String, Integer> outDegreeMap;
	
	
	public BlazegraphStatisticsWithDatabaseSupportTest () {
		edgeSetCardinality = 0;
		labelCounterMap = new HashMap<String, Integer>();
		inDegreeMap = new HashMap<String, Integer>();
		outDegreeMap = new HashMap<String, Integer>();
	}
	
	/** Crates the statistics of a graph, saving them in memory.
	 * 
	 * @param rdfDatabase Path to the RDF database to query
	 * @param jdbcDatabase path to the database to be used. An example is: 
	 * jdbc:postgresql://localhost:5432/disgenet?user=postgres&password=password
	 * @param jdbcDriver what driver to use. An example is com.mysql.jdbc.Driver.
	 * */
	public void getGraphStatistics(String rdfDataset, String jdbcDatabase, String jdbcDriver) {
		
		//prepare RDB connection
		try {
			Class.forName(jdbcDriver);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		Connection connection = null;
		RepositoryConnection cxn = null;
		
		try {
			//open RDB connection
			connection = DriverManager.getConnection(jdbcDatabase);
			
			//open connection to RDF dataset
			Properties props = new Properties();
			props.put(Options.BUFFER_MODE, "DiskRW");
			props.put(Options.FILE, rdfDataset);
			final BigdataSail sail = new BigdataSail(props); // instantiate a sail
			final Repository repo = new BigdataSailRepository(sail); // create a Sesame repository
			//open the connection to the database (remember to shut it off at the end)
			repo.initialize();
			cxn = BlazegraphUsefulMethods.getRepositoryConnection(repo);
			
			//get an iterator over the RDF dataset 
			TupleQueryResult iterator = BlazegraphUsefulMethods.getIterator(cxn);
			System.out.println("Got iterator over RDF dataset. Start navigating...");
			
			//monitor thread
			final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
			scheduler.scheduleAtFixedRate(new MonitorStatisticsThread(), 10, 30, TimeUnit.SECONDS);
			
			int progressCounter = 0;
			//start the iteration to collect information
			while(iterator.hasNext()) {
				edgeSetCardinality++;
				progressCounter++;
				//get the triple
				BindingSet bs = iterator.next();
				
				//take subject, predicate and object
				Value subject = bs.getValue("s");
				Value predicate = bs.getValue("p");
				Value object = bs.getValue("o");
				
				//update the label counter
				String label = predicate.toString();
				MapsUsefulMethods.updateSupportMap(predicate.stringValue(), this.labelCounterMap);
				//update in degree and out degree of nodes
				//the subject has +1 as out degree, the object +1 as in degree
				MapsUsefulMethods.updateSupportMap(subject.stringValue(), this.outDegreeMap);
				MapsUsefulMethods.updateSupportMap(object.stringValue(), this.inDegreeMap);
				
				
				
				//update the database
				if(progressCounter > 100000) {
					progressCounter = 0;
					System.out.println("updating the database...");
					SQLUtilities.batchUpdateLabelFrequencyIntoDatabaseFromMap(connection, this.labelCounterMap);
					SQLUtilities.batchUpdateDegreeInDatabaseFromMaps(connection, this.outDegreeMap, this.inDegreeMap, true);
				}
			}//end of iteration over the edges
			
			//clean-up 
			iterator.close();
			scheduler.shutdownNow();
			
			
		} catch (SQLException e) {
			
			System.err.println("DEBUG: SQL error");
			e.printStackTrace();
		
		} catch (RepositoryException e) {
			System.err.println("error initializing RDF connection to database");
			e.printStackTrace();
		} catch (QueryEvaluationException e) {
			System.err.println("Debug: error in valiuating hasNext() on the TupleQueryResult iterator");
			e.printStackTrace();
		} finally {
			try {
				if (connection != null)
					connection.close();
				if(cxn != null) {
					cxn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (RepositoryException e) {
				System.err.println("Error in closing RDF connection");
				e.printStackTrace();
			}
		}
	}
	
	
	
	/** Thread to monitor how fast the algorithm is going.
	 * 
	 * */
	private class MonitorStatisticsThread extends Thread {
		public void run() {
			System.out.println("Read " + BlazegraphStatisticsWithDatabaseSupportTest.this.edgeSetCardinality + " edges");
		}
	}
}
