package it.unipd.dei.ims.blazegraph.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.openrdf.query.BindingSet;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.query.TupleQuery;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;

import com.bigdata.journal.Options;
import com.bigdata.rdf.sail.BigdataSail;
import com.bigdata.rdf.sail.BigdataSailRepository;

/** Reads from a triple database and stores inside a relational database.
 * */
public class BlazegraphIteratorAndUpdaterTest implements Runnable {
	private static int counter = 0;
	private static Connection connection = null;

	public static void main(String[] args) throws RepositoryException {

		//string to connect to the database
		String connectionString = "jdbc:mysql://localhost:3306/DisGeNET?user=root&password=Ulisse92";
		//connection to the database
		try {
		    Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
		    e.printStackTrace();
		}
		
		final Properties props = new Properties();
		props.put(Options.BUFFER_MODE, "DiskRW"); // persistent file system located journal
		props.put(Options.FILE, "/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/TDB_sesame/test.jnl"); // journal file location

		final BigdataSail sail = new BigdataSail(props); // instantiate a sail
		final Repository repo = new BigdataSailRepository(sail); // create a Sesame repository
		
		//connetcion to the RDB
		//repository to the TDB
		repo.initialize();

		try {
			// open repository connection
			RepositoryConnection cxn;

			// open connection
			if (repo instanceof BigdataSailRepository) {
				cxn = ((BigdataSailRepository) repo).getReadOnlyConnection();
			} else {
				cxn = repo.getConnection();
			}
			
			//open connection to RDB
			connection = DriverManager.getConnection(connectionString);
			PreparedStatement prepared = connection.prepareStatement("update RDF_DATABASE set triple_number = triple_number + 1 WHERE name='DisGeNET' ");
			
			// evaluate sparql query
			try {

				final TupleQuery tupleQuery = cxn
						.prepareTupleQuery(QueryLanguage.SPARQL,
								"select ?s ?p ?o where { ?s ?p ?o . }");
				
				TupleQueryResult result = tupleQuery.evaluate();
				
				final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
				scheduler.scheduleAtFixedRate(new BlazegraphIteratorAndUpdaterTest(), 10, 10, TimeUnit.SECONDS);
				
				counter = 0;
				try {
					while (result.hasNext()) {
						//NB you have to call next, otherwise the iterator stays in the same position
						BindingSet bindingSet = result.next();
						//execute SQL update
						counter++;
						prepared.executeUpdate();
						prepared.executeUpdate();
						prepared.executeUpdate();
						if(counter > 100000) {
							break;
						}
					}
					connection.commit();
					System.out.println("triples: " + counter);
				} finally {
					result.close();
				}
				scheduler.shutdownNow();

			} catch (MalformedQueryException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (QueryEvaluationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				// close the repository connection
				cxn.close();
			}

		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} finally {
			repo.shutDown();
			try {
		        if (connection != null)
		            connection.close();
		    } catch (SQLException e) {
		        // gestione errore in chiusura
		    }
		}
		
	}
	
		public void run() {
			System.out.println("Controlled " + counter + " triples");
//			try {
//				connection.commit();
//			} catch (SQLException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
		}
}
